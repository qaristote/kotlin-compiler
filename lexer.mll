(* Analyseur lexical pour Petit Kotlin *)

{
  open Lexing
  open Constants
  open Ast
  open Parser

  exception Lexing_error of string

  let id_or_kwd =
    let h = Hashtbl.create 17 in
    List.iter (fun (s, tok) -> Hashtbl.add h s tok)
      ["class", CLASS;
       "data", DATA;
       "else", ELSE;
       "false", CST (Cbool false);
       "fun", FUN;
       "if", IF;
       "null", CST Cnull;
       "return", RETURN;
       "this", CST Cthis;
       "true",  CST (Cbool true);
       "val", VAL;
       "var", VAR;
       "while", WHILE];
   fun s -> try Hashtbl.find h s with Not_found -> IDENT s

   let rec pow a = function
       | 0 -> 1
       | p -> let sqrt = pow a (p/2) in
       	      sqrt * sqrt * (if p mod 2 = 0 then 1 else a)      

  let string_buffer = Buffer.create 1024
}

let digit = ['0'-'9']
let alpha = ['a'-'z' 'A'-'Z' '_']
let ident = alpha (alpha | digit)*
let bit = '0' | '1'
let hexa = digit | ['a'-'f' 'A'-'F']
let integer = digit
         | digit (digit | '_')* digit
         | ("0x" | "0X") (hexa | hexa (hexa | '_')* hexa)
         | ("0b" | "0B") (bit | bit (bit | '_')* bit)
let character = digit | alpha | [' ' '!' '#' '$' '%' '&' '\'' '(' ')' '*' '+'
    	      	 ',' '-' '.' '/' ':' ';' '<' '=' '>' '?' '@' '['  ']' '^' '`'
		 '{' '|' '}' '~'] | "\\\"" | "\\\\" | "\\n" | "\\t"
let space = ' ' | '\t'
let line_comment = "//" [^'\n']*

rule token = parse
  | '\n'		{ new_line lexbuf ; token lexbuf }
  | (space
    | line_comment)+   	{ token lexbuf }
  | "/*"		{ comment lexbuf; token lexbuf }
  | ident as id	      	{ id_or_kwd id }
  | '+'      		{ PLUS }
  | '-'     		{ MINUS }
  | '*'    		{ TIMES }
  | "/"   		{ DIV }
  | '%'     		{ MOD }
  | "||"    		{ OR }
  | "&&"    		{ AND }
  | '!'     		{ NOT }
  | '='     		{ EQUAL }
  | "==="   		{ IS }
  | "!=="               { NIS }
  | "=="    		{ EQ }
  | "!="    		{ NEQ }
  | "<"     		{ LT }
  | "<="    		{ LEQ }
  | ">"     		{ GT }
  | ">="		{ GEQ }
  | '('     		{ LP }
  | ')'     		{ RP }
  | '{'     		{ LB }
  | '}'     		{ RB }
  | ','     		{ COMMA }
  | ':'     		{ COLON }
  | ';'     		{ SEMICOLON }
  | '.'     		{ ATT }
  | "?."    		{ OPTATT }
  | "->"    		{ YIELDS }
  | '?'     		{ OPT }
  | integer as s	{ try
    	       		    let n = int_of_string s in
			    let max = pow 2 31 in
			    if n >= max - 1 then
			       raise (Lexing_error ("integer overflow : "^s))
			    else
				CST (Cint n)
			  with
			    Failure _ ->
			      raise (Lexing_error ("integer overflow : "^s)) }
  | '"'    		{ CST (Cstring (string lexbuf)) }
  | eof     		{ EOF }
  | _ as c  		{ raise (Lexing_error 
      	 		  	   ("illegal character : " ^ String.make 1 c)) }

and comment = parse
  | "*/"		{ () }
  | "/*" 		{ comment lexbuf ; 
    			  comment lexbuf }
  | '\n'		{ new_line lexbuf ;
  			  comment lexbuf }			  
  | _ 			{ comment lexbuf }
  | eof 		{ raise (Lexing_error ("unterminated comment")) }

and string = parse
  | '"'			{ let s = Buffer.contents string_buffer in
			  Buffer.reset string_buffer;
			  s }
  | "\\n"		{ Buffer.add_char string_buffer '\n' ;
			  string lexbuf }
  | "\\\""		{ Buffer.add_char string_buffer '"' ;
    			  string lexbuf }
  | character as c	{ Buffer.add_string string_buffer c ;
	      	 	  string lexbuf }
  | eof			{ raise (Lexing_error "unterminated string") }
  | _ as c		{ raise (Lexing_error 
      	 		  	   ("illegal character in string : " 
				    ^ (if c = '\n' then
				      	  "\\n"
				       else
					  String.make 1 c)))}
