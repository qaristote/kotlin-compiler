(* Analyseur syntaxique pour Petit Kotlin *)

%{ open Ast
   open Constants
   let by_default d = function
     | Some x -> x
     | None -> d %}

%token <Ast.cst> CST
%token IS NIS LT LEQ EQ NEQ GEQ GT
%token <Ast.typ> TYPE
%token <string> IDENT
%token FUN IF ELSE RETURN WHILE AND OR NOT DATA CLASS VAR VAL
%token EOF
%token LP RP LB RB COMMA EQUAL COLON SEMICOLON
%token PLUS MINUS TIMES DIV MOD
%token OPT OPTATT ATT YIELDS

(* Définitions des priorités et associativités des tokens *)

%nonassoc RETURN
%nonassoc no_else
%nonassoc ELSE
%right EQUAL
%left OR
%left AND
%left IS NIS EQ NEQ
%left GT GEQ LT LEQ
%left PLUS MINUS
%left TIMES DIV MOD
%right NOT
%left ATT OPTATT
%right YIELDS
%nonassoc OPT

(* Point d'entrée de la grammaire *)
%start file

(* Type des valeurs renvoyées par l'analyseur syntaxique *)
%type <Ast.file> file

%%

file:
  | dl = list(decl) EOF
    { dl }
;

decl:
  | v = var SEMICOLON
    { Dvar v }
  | c = clas
    { Dclass c }
  | f = func
    { Dfun f }
;

var:
  | v = varval id = IDENT t = poption(COLON, typ) EQUAL e = expr
    { {v_loc = ($startpos, $endpos) ;
       v_name = id ;
       v_type = by_default type_any t ;
       v_expr = e ;
       v_is_mutable = v} }
;


clas:
  | DATA CLASS name = IDENT
type_params = dlist(LT, IDENT, GT, COMMA)
LP params = separated_nonempty_list(COMMA, param_c) RP
vars = fields
    { { c_loc = ($startpos, $endpos) ;
	c_name = name ;
	c_type_params = Array.of_list type_params ;
	c_params = params ;
	c_body = vars} }
;

fields:
  | (* epsilon *)
    { [] }
  | LB RB
    { [] }
  | LB vars = rev_separated_nonempty_list(SEMICOLON, var) ioption(SEMICOLON) RB
    { List.rev vars }
;

func:
  | FUN types = dlist(LT, IDENT, GT, COMMA)
name = IDENT
LP params = separated_list(COMMA, param) RP
t = poption(COLON, typ)
b = bloc
    { { f_loc = ($startpos, $endpos) ;
	f_type_params = Array.of_list types ;
	f_name = name ;
	f_params = params ;
	f_type = by_default type_unit t ;
	f_body = b} }
;

param:
  | id = IDENT COLON t = typ
    { id, t }
;

param_c:
  | v = varval p = param
    { p, v }
;

typ:
  | t = TYPE
    { t }
  | id = IDENT types = dlist(LT, typ, GT, COMMA)
    { Tclass (id, Array.of_list types) }
  | t = typ OPT
    { type_opt t }
  | LP t = typ RP
    { t }
  | LP RP YIELDS t = typ
    { Tfun ([], t) }
  | LP t1 = typ RP YIELDS t2 = typ
    { Tfun ([t1], t2) }
  | LP t1 = typ COMMA types = separated_nonempty_list(COMMA, typ) RP
YIELDS t2 = typ
    { Tfun (t1::types, t2) }
;

expr:
  | i = instr
    { {e_loc = ($startpos, $endpos) ;
       e_type = type_any ;
       e_instr = i} }
  | LP e = expr RP
    { e }
;

%inline instr:
  | c = CST
    { Ecst c }
  | g = get
    { Eget g }
  | g = get EQUAL e = expr
    { Eset (g, e) }
  | name = IDENT LP args = separated_list(COMMA, expr) RP
    { Ecall (name, args) }
  | o = unop e = expr
    { Eunop (o, e) }
  | e1 = expr o = binop e2 = expr
    { Ebinop (o, e1, e2) }
  | IF LP e = expr RP b1 = blocexpr %prec no_else
    { Eif (e, b1, {b_loc = ($startpos, $endpos) ;
		   b_type = type_unit ;
		   b_instr = []}) }
  | IF LP e = expr RP b1 = blocexpr ELSE b2 = blocexpr
    { Eif (e, b1, b2) }
  | WHILE LP e = expr RP b = blocexpr
    { Ewhile (e, b) }
  | RETURN e = ioption(expr)
    { Ereturn e }
  | FUN LP params = separated_list(COMMA, param) RP
t = poption(COLON, typ)
b = bloc
    { Efun (params, by_default type_unit t, b) }
;

bloc:
  | LB instrs = varexprs RB
    { { b_loc = ($startpos, $endpos) ;
	b_type = type_any ;
	b_instr = List.rev instrs} }
;

%inline varexprs:
  | (* epsilon *)
    { [] }
  | vs = rev_separated_nonempty_list(SEMICOLON, varexpr) ioption(SEMICOLON)
    { vs }
;

varexpr:
  | v = var
    { Var v }
  | e = expr
    { Expr e }
;

blocexpr:
  | b = bloc
    { b }
  | e = expr %prec TIMES
    { { b_loc = e.e_loc ;
	b_type = e.e_type ;
	b_instr = [Expr e] } }
;

get:
  | id = IDENT
    { Gident id }
  | e = expr ATT id = IDENT
    { Gatt (e, id) }
  | e = expr OPTATT id = IDENT
    { Goptatt (e, id) }
;

%inline binop:
  | PLUS
    { Badd }
  | MINUS
    { Bsub }
  | TIMES
    { Bmul }
  | DIV
    { Bdiv }
  | MOD
    { Bmod }
  | AND
    { Band }
  | OR
    { Bor }
  | LT
    { Blt }
  | LEQ
    { Ble }
  | EQ
    { Beq }
  | NEQ
    { Bneq }
  | GEQ
    { Bge }
  | GT
    { Bgt }
  | IS
    { Bis }
  | NIS
    { Bnis }
;

%inline unop:
  | MINUS
    { Uneg }
  | NOT
    { Unot }

%inline varval:
  | VAR
    { true }
  | VAL
    { false }
;

(* équivalent à ioption(preceded(prec, X)) *)
%public %inline poption(prec, X) :
  | prec x = X
    { Some x }
  | (* epsilon *)
    { None }

(* liste qui, lorsque non-vide, est délimitée par des caractères spécifiques *)
%public dlist(left, X, right, sep): 
  | xs = loption(delimited(left, separated_nonempty_list(sep, X), right))
    { xs }

(* liste construite à l'envers ; la complexité reste la même mais on peut 
   maintenant corriger les problèmes venant du fait que la grammaire doit être 
   dans LALR(1) *)
%public rev_separated_nonempty_list(sep, X):
  | xs = rev_separated_nonempty_list(sep, X) sep x = X
    { x::xs }
  | x = X
    { [x] }
