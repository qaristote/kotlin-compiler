fun fold(todo: (Int) -> Int) {
    var x = 0;
    var r = 0;
    while (x < 10) {
        r = todo(r);
        x = x+1;
    };
    print(r);
    print("\n");
}

// construire une fermeture
fun add(x: Int) : (Int) -> Int {
    return fun (y: Int) : Int { return x+y; }
}

fun main(args: Array<String>) {
    val foo = fun (x: Int) : Int { return x+1; };
    fold(foo);
    fold(fun (x: Int) : Int { return x+2; });
    fold(add(3));
}
