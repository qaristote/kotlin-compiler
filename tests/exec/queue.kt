
/* Files applicatives, avec une paire de listes */

data class List<T>(val head: T, val tail: List<T>?)

data class Queue<T>(val front: List<T>?, val rear: List<T>?)

fun<T> add(x: T, q: Queue<T>) : Queue<T> {
    return Queue(q.front, List(x, q.rear));
}

fun<T> rev_append(l1: List<T>?, l2: List<T>?) : List<T>? {
    if (l1 === null) return l2
    else return rev_append(l1.tail, List(l1.head, l2));
}

fun<T> reverse(l: List<T>?) : List<T>? {
    return rev_append(l, null);
}

// renvoie null si la file est vide
fun<T> head(q: Queue<T>) : T? {
    val f = q.front;
    return if (f !== null) f.head else reverse(q.rear)?.head;
}

// renvoie une file vide si la file est vide
fun<T> tail(q: Queue<T>) : Queue<T> {
    val f = q.front;
    if (f !== null) return Queue(f.tail, q.rear)
    else return Queue(reverse(q.rear)?.tail, null)
}

fun main(args: Array<String>) {
    val l: List<String>? = null;
    var q = Queue(l, null);
    q = add("1", q);
    q = add("2", q);
    q = add("3", q);
    print(head(q)); print("\n");
    q = tail(q);
    print(head(q)); print("\n");
    q = tail(q);
    print(head(q)); print("\n");
}
