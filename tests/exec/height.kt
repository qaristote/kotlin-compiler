
/* Calculer la hauteur d'un arbre */

data class Tree(val left: Tree?, val right: Tree?)

fun max(a: Int, b: Int) : Int {
    if (a > b) return a else return b;
}

fun height(t: Tree?) : Int {
    if (t === null) return 0;
    return 1 + max(height(t.left), height(t.right));
}

fun linear(h: Int) : Tree? {
    if (h == 0) return null;
    return Tree(linear(h-1), null);
}

/* efficacement, sans faire déborder la pile

   cela étant, kotlinc et kotlinc-native n'optimisent pas bien l'appel
   terminal : d'une part, il faut indiquer "tailrec" devant la fonction ;
   d'autre part, le second appel à heightCPS n'est pas reconnu comme
   terminal ; enfin, les deux appels à k ne le sont pas non plus...
*/

fun heightCPS(t: Tree?, k: (Int) -> Int) : Int {
    if (t === null) return k(0);
    return heightCPS(t.left,  fun (hl: Int) : Int {
    return heightCPS(t.right, fun (hr: Int) : Int {
    return k(1 + max(hl, hr));                    })});
}

fun height2(t: Tree?) : Int {
    return heightCPS(t, fun (h: Int) : Int { return h; });
}

fun main(args: Array<String>) {
    print(height(null)); print("\n");
    val t2: Tree = Tree(Tree(null,null), null);
    print(height(t2)); print("\n");
    print(height(linear(42))); print("\n");
    print(height2(linear(42))); print("\n");
}
