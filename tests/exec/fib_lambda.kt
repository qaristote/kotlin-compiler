
// calcule x^n*y par exponentiation rapide
fun<A> exp(m: (A,A)->A, x: A, n: Int, y: A) : A {
    if (n == 0) return y;
    return exp(m, m(x,x), n/2, if (n % 2 == 1) m(x,y) else y)
}

data class Pair(val a: Int, val b: Int)
// représente la matrice 2x2
//   a+b b
//    b  a

val m: Int = 10_000; // on calcule modulo 10^4

fun add(x: Int, y: Int) : Int { return (x+y) % m; }
fun mul(x: Int, y: Int) : Int { return (x*y) % m; }

fun fib(n: Int) : Int {
    val mulp = fun (x: Pair, y: Pair) : Pair {
        return Pair(add(mul(x.b,y.b),mul(x.a,y.a)),
                    add(mul(add(x.a,x.b),y.b), mul(x.b,y.a)));
    };
    val i = Pair(0,1);
    val p = exp(mulp, i, n-1, i);
    return p.b;
}

fun main(args: Array<String>) {
    print(exp(fun (a:Int, b:Int) : Int { return a*b }, 2, 30, 1)); print("\n");
    print(exp(fun (a:Int, b:Int) : Int { return a*b }, 2, 31, 1)); print("\n");
    print(fib(10)); print("\n");
    print(fib(2014)); print("\n");
    print(fib(1_000_000_000)); print("\n");
}
