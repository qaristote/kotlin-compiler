
// tri fusion en place de listes simplements chaînées

data class Singly(val head: Int, var next: Singly?)

fun split(l: Singly?) : Singly? {
    var n = 0;
    var l1 = l;
    while (l1 !== null) { l1 = l1.next; n = n+1; };
    n = (n-1)/2;
    var last = l;
    while (n > 0) { last = last?.next; n = n-1; };
    val l2 = last?.next;
    last?.next = null;
    return l2;
}

fun merge(a: Singly?, b: Singly?) : Singly? {
    if (a === null) return b;
    if (b === null) return a;
    if (a.head > b.head) return merge(b, a);
    val res = a; // pointeur sur le tout premier element
    var l1: Singly? = a;
    var l2: Singly? = b;
    // invariant res->...->l1->...->null
    //                     l2->...->null
    //        et l1.head <= l2.head
    while (l2 !== null) {
        val v = l2.head;
        var n = l1?.next;
        while (n !== null && n.head <= v) { l1 = n; n = n?.next };
        l1?.next = l2;
        l2 = n;
    };
    return res;
}

fun mergesort(l: Singly?) : Singly? {
    if (l === null || l.next === null) return l;
    val l2 = split(l);
    return merge(mergesort(l), mergesort(l2));
}

fun print_list(l: Singly?) {
    if (l === null) { print("\n"); return; };
    print(l.head); print(" ");
    print_list(l.next);
}

var state = 0;
fun random() : Int { state = (state * 987 + 42) % 5003; return state; }

fun random_list(n: Int) : Singly? {
    if (n == 0) return null;
    return Singly(random(), random_list(n - 1));
}

fun main(args: Array<String>) {
    val l = Singly(4, Singly(1, Singly(2, Singly(5, Singly(3, null)))));
    print_list(l);
    print_list(mergesort(l));
    val l2 = random_list(100);
    print_list(l2);
    print_list(mergesort(l2));
}
