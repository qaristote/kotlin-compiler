data class List(val h: Int, val n: List?)

fun length(l: List?) : Int {
    if (l === null) return 0;
    return 1 + length(l.n);
}

fun main(args: Array<String>) {
    var l = List(1, List(2, List(3, null)));
    print(length(l)); print("\n")
}

