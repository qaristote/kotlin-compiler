fun main(args: Array<String>) {
    val expr1 = - 7 + 6 * 5 - 4 / 3 - 2;     // 20
    val expr2 = (7 + 6) * (5 - 4) / 3 - 2; // 2
    val expr3 = 7 + 6 * (5 - 4 / (3 - 2));  // 13
    val sum = expr1 % 8 + expr2 + expr3;    // 19
    print(sum); print("\n")
}
