data class A(var x: Int)

fun main(args: Array<String>) {
    var a: A? = null;
    a?.x = 42;
    print("OK\n")
}
