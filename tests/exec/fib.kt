
fun fib_iter(max: Int) {
    var a = 0;
    var b = 1;
    while (a <= max) {
        print(a); print(" ");
        b = a + b;
        a = b - a;
    }
}

fun fib_rec(a: Int, b: Int, max: Int) {
    if (a > max) return;
    print(a); print(" ");
    fib_rec(b, a+b, max);
}

fun main(args: Array<String>) {
    fib_iter(1000); print("\n");
    fib_rec(0, 1, 1000); print("\n");
}
