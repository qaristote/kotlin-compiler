fun m(s: String) { print(s) }

fun main(args: Array<String>) {
    m("hello");
    m(", world!\n")
}
