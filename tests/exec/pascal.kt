
// triangle de Pascal modulo 7

data class List(var value: Int, val next: List?)

fun get(l: List?, i: Int) : Int {
    if (l === null) return -1; // ne doit pas arriver
    if (i == 0) return l.value;
    return get(l.next, i-1)
}

fun set(l: List?, i: Int, v: Int) {
    if (l === null) return;
    if (i == 0) l.value = v
    else set(l.next, i-1, v)
}

fun print_row(r: List?, i: Int) {
    var j = 0;
    while (j <= i) {
      if (get(r, j) != 0)
        print("*")
      else
        print("0");
      j = j+1
    };
    print("\n")
}

fun compute_row(r: List?, j: Int) {
    var v = 0;
    if (j == 0)
      v = 1
    else
      v = (get(r, j) + get(r, j-1)) % 7;
    set(r, j, v);
    if (j > 0)
      compute_row(r, j-1)
}

fun create(n: Int) : List? {
    if (n == 0) return null;
    return List(0, create(n-1))
}

fun main(args: Array<String>) {
    val h = 42;
    val r = create(h+1);
    var i = 0;
    while (i < h) {
      set(r, i, 0);
      compute_row(r, i);
      print_row(r, i);
      i = i+1
    }
}
