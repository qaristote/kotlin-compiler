fun print_bool(b: Boolean) {
    print(if (b) "true" else "false");
    print("\n")
}

fun main(args: Array<String>) {
    print_bool(true);
    print_bool(false);
    print_bool(! true);
    print_bool(! false)
}
