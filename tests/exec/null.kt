data class A(val x: Int)

fun main(args: Array<String>) {
    var a: A? = A(42);
    if (null !== null) print("oups\n");
    if (null !== a) print("yes!\n");
    if (null === "toto") print("oups\n");
    a = null;
    if (a === null) print("OK\n")
}
