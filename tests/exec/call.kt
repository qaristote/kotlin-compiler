var s : String? = null;

fun set(x: String) { s = x }

fun get() : String? { return s }

fun main(args: Array<String>) {
    set("hello");
    print(get());
    print(", world!\n")
}
