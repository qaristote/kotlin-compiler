/** listes circulaires doublement chaînées */

data class ListeC(val valeur: Int) {
    var suivant: ListeC = this;
    var precedent: ListeC = this;
}

/* insertion après un élément donnée */
fun insererApres(l: ListeC, v: Int) {
    var e = ListeC(v);
    e.suivant = l.suivant;
    l.suivant = e;
    e.suivant.precedent = e;
    e.precedent = l
}

/* suppression d'un élément donné */
fun supprimer(l: ListeC) {
    l.precedent?.suivant = l.suivant;
    l.suivant?.precedent = l.precedent
}

  /* affichage */
fun afficher(l: ListeC) {
    var c: ListeC = l;
    print(c.valeur);
    print(" ");
    c = c.suivant;
    while (c !== l) {
      print(c.valeur);
      print(" ");
      c = c.suivant
    };
    print("\n")
}


/** Partie 3 : problème de Josephus */

/* construction de la liste circulaire 1,2,...,n;
   l'élément retourné est celui contenant 1 */
fun cercle(n: Int) : ListeC {
    var l = ListeC(1);
    var i = n;
    while (i >= 2) {
      insererApres(l, i);
      i = i-1
    };
    return l
}

  /* jeu de Josephus */
fun josephus(n: Int, p: Int) : Int {
    /* c est le joueur courant, 1 au départ */
    var c = cercle(n);

    /* tant qu'il reste plus d'un joueur */
    while (c !== c.suivant) {
      /* on élimine un joueur */
      var i = 1;
      while (i < p) { c = c.suivant; i = i + 1 };
      supprimer(c);
      c = c.suivant
    };
    return c.valeur
}

/*** Tests */

fun main(args: Array<String>) {
    var l = ListeC(1);
    afficher(l);
    insererApres(l, 3);
    afficher(l);
    insererApres(l, 2);
    afficher(l);
    supprimer(l.suivant);
    afficher(l);

    var c = cercle(7);
    afficher(c);

    if (josephus(7, 5) == 6 &&
	josephus(5, 5) == 2 &&
	josephus(5, 17) == 4 &&
	josephus(13, 2) == 11)
      print("ok\n")
}
