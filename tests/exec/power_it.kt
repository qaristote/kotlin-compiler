fun power(a: Int, n: Int) : Int {
    var r = 1;
    var p = a;
    var e = n;
    while (e > 0) {
      if (e % 2 != 0) r = r * p;
      p = p * p;
      e = e / 2
    };
    return r
}

fun main(args: Array<String>) {
    print(power(2, 4)); print("\n");
    print(power(6, 3)); print("\n")
}
