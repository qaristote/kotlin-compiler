fun fact(n: Int) : Int {
    if (n <= 1)
      return 1
    else
      return n * fact(n-1)
}

fun main(args: Array<String>) {
    print(fact(5)); print("\n");
    print(fact(11)); print("\n")
}
