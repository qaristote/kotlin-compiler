data class BST(var left: BST?, val value: Int, var right: BST?)

fun add(self: BST, x: Int) {
    if (x == self.value) return;
    if (x < self.value) {
        val node = self.left;
        if (node === null)
	    self.left = BST(null, x, null)
        else
	    add(node, x);
    } else {
        val node = self.right;
        if (node === null)
	    self.right = BST(null, x, null)
        else
   	    add(node, x)
    }
}

fun contains(self: BST?, x: Int) : Boolean {
    if (self === null) return false;
    if (x == self.value) return true;
    if (x < self.value) return contains(self.left, x)
    else return contains(self.right, x);
}

fun display(self: BST?) {
    if (self === null) return;
    display(self.left);
    print(" "); print(self.value); print(" ");
    display(self.right)
}

fun main(args: Array<String>) {
    val dico: BST = BST(null, 1, null);
    add(dico, 17);
    add(dico, 5);
    add(dico, 8);
    display(dico); print("\n");

    if (contains(dico, 5) &&
	! contains(dico, 0) &&
	contains(dico, 17) &&
	! contains(dico, 3))
      print("ok\n")
  }

