fun diff(a: Int, b: Int, c: Int) : Int { // a\b\c a&~b&~c
    if (a == 0) return 0;
    return 2 * diff(a/2, b/2, c/2) + if (a%2==1 && b%2==0 && c%2==0) 1 else 0;
}
fun lowest_bit(x: Int) : Int {
    if (x == 1) return 1;
    return 2 * lowest_bit(x / 2);
}

fun t(a: Int, b: Int, c: Int) : Int {
    var f = 1;
    if (a > 0) {
        f = 0;
        var e = diff(a, b, c);
        while (e > 0) {
            val d = lowest_bit(e);
            f = f + t(a-d, (b+d)*2, (c+d)/2);
            e = e - d;
        }
    };
    return f;
}

fun low_bits(n: Int) : Int {
    if (n == 0) return 0;
    return 2 * low_bits(n-1) + 1;
}

fun q(n: Int) : Int {
    return t(low_bits(n), 0, 0);
}

fun main(args: Array<String>) {
    var n = 1;
    while (n <= 10) {
        print(q(n)); print("\n");
        n = n+1;
    }
}
