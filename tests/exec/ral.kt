/* Random access lists (Okasaki, 1995)
 *
 * Un exemple utilisant de la récusion polymorphe
 */

data class Pair<A, B>(val fst: A, val snd: B)

// le booléen one indique si l'élément e est significatif
data class List<E>(val one: Boolean, val e: E, val next: List<Pair<E, E>>?)

fun<E> length(l: List<E>?) : Int {
    if (l === null) {
        return 0;
    } else {
        return 2 * length(l.next) + (if (l.one) 1 else 0);
    }
}

fun<E> get(l: List<E>?, i: Int) : E {
    if (l === null) {
        print("no such element\n");
        return get(l, i);
    };
    if (l.one) {
        if (i == 0) return l.e;
        val x: Pair<E, E> = get(l.next, (i - 1) / 2);
        return if (i % 2 == 1) x.fst else x.snd
    } else {
        val x: Pair<E, E> = get(l.next, i / 2);
        return if (i % 2 == 0) x.fst else x.snd
    }
}

fun<E> add(l: List<E>?, x: E) : List<E> {
    if (l === null) {
        return List(true, x, null);
    } else if (l.one) {
        return List(false, x, add(l.next, Pair(x, l.e)));
    } else {
        return List(true, x, l.next);
    }
}

fun sequence(i: Int, j: Int) : List<Int>? {
    if (j < i)
        return null
    else {
        val l = sequence(i + 1, j);
        return add(l, i)
    }
}

fun main(args: Array<String>) {
    val s: List<Int>? = sequence(1, 10);
    var i = 0;
    while (i < 10) {
        print(get(s, i));
        print(" ");
        i = i+1
    };
    print("\n")
}

