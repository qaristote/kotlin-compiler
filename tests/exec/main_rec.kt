var n = 999;
var s = 0;

fun test() : Boolean { return n % 3 == 0 || n % 5 == 0; }

fun main(args: Array<String>) {
    if (n == 0) { print(s); print("\n") }
    else { if (test()) s = s + n; n = n - 1; main(args) }
}
