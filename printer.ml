open Ast
open Format

let needs_parenthesis = function
  | Eget (Gident _) | Ecst _ | Ecall _-> false
  | _ -> true

let print_ident ff = fprintf ff "%s"

let rec print_separated_list foo sep ff l =
  let rec aux ff = function
    | [] -> fprintf ff "@]"
    | [el] -> fprintf ff "%a@]" foo el
    | t::q -> fprintf ff "%a%a%a" foo t fprintf sep aux q
  in fprintf ff "@[%a" aux l
          
let print_unop ff unop =
  fprintf ff "%s"
    (match unop with
     | Uneg -> "-"
     | Unot -> "!")

let print_binop ff binop =
  fprintf ff "%s"
    (match binop with
     | Badd -> "+"
     | Bsub -> "-"
     | Bmul -> "*"
     | Bdiv -> "/"
     | Bmod -> "%"
     | Bis -> "==="
     | Bnis -> "!=="
     | Beq -> "=="
     | Bneq -> "!="
     | Blt -> "<"
     | Ble -> "<="
     | Bgt -> ">"
     | Bge -> ">="
     | Band -> "&&"
     | Bor -> "||")

let print_cst ff = function
  | Cnull -> fprintf ff "null"
  | Cthis -> fprintf ff "this"
  | Cint n -> fprintf ff "%d" n
  | Cstring s ->
     fprintf ff "\"%a\""
       (fun fmt s -> String.iter
                       (fun c -> match c with
                                 | '\n' -> fprintf fmt "\\n"
                                 | '\t' -> fprintf fmt "\\t"
                                 | '"' -> fprintf fmt "\\\""
                                 | '\'' -> fprintf fmt "\\'"
                                 | '\\' -> fprintf fmt "\\"
                                 | _ -> fprintf fmt "%s" (String.make 1 c)) s) s
  | Cbool b -> fprintf ff "%B" b

let rec print_get ff = function
  | Gident id -> print_ident ff id
  | Gatt (e, id) ->
     fprintf ff
       (if needs_parenthesis e.e_instr then
          "@[(%a).%a@]"
        else
          "@[%a.%a@]")
       print_expr e
       print_ident id
  | Goptatt (e, id) ->
     fprintf ff
       (if needs_parenthesis e.e_instr then
          "@[%a?.%a@]"
        else
          "@[(%a)?.%a@]")
       print_expr e
       print_ident id

and print_varexpr ff = function
  | Var v -> print_var ff v
  | Expr e -> print_expr ff e

and print_bloc ff b =
  fprintf ff "{@.@[<v 1>" ;
  print_separated_list print_varexpr " ;@." ff b.b_instr ;
  close_tbox () ;
  fprintf ff "@]@.}"

and print_expr ff expr = match expr.e_instr with
  | Ecst c -> print_cst ff c
  | Eget g -> print_get ff g
  | Eset (g, e) -> fprintf ff "@[%a = %a@]" print_get g print_expr e
  | Ecall (id, l) ->
     fprintf ff "@[%s(%a)@]"
       id
       (print_separated_list print_expr ", ") l
  | Eunop (u, e) ->
     fprintf ff (if needs_parenthesis e.e_instr then
                   "@[%a(%a)@]"
                 else
                   "@[%a%a@]")
       print_unop u
       print_expr e
  | Ebinop (b, e1, e2) ->
     fprintf ff
       (match needs_parenthesis e1.e_instr, needs_parenthesis e2.e_instr with
        | true, true -> "@[(%a) %a (%a)@]"
        | true, false -> "@[(%a) %a %a@]"
        | false, true -> "@[%a %a (%a)@]"
        | false, false -> "@[%a %a %a@]")
       print_expr e1
       print_binop b
       print_expr e2
  | Eif (e, b1, b2) ->
     if b2.b_instr = [] then
       fprintf ff "@[if (%a) %a@]"
         print_expr e
         print_bloc b1
     else
       fprintf ff "@[if (%a) %a else %a@]"
         print_expr e
         print_bloc b1
         print_bloc b2
  | Ewhile (e, b) ->
     fprintf ff "@[while (%a) %a@]"
       print_expr e
       print_bloc b
  | Ereturn (Some e) -> fprintf ff "@[return %a@]" print_expr e
  | Ereturn None -> fprintf ff "return"
  | Efun (l, t, b) ->
     fprintf ff "@[fun (%a) : %a %a]"
       (print_separated_list print_param ", ") l
       print_type t
       print_bloc b

and print_type ff = function
  | Tclass (id, [||]) -> print_ident ff id
  | Tclass (id, a) ->
     fprintf ff "@[%s<%a>@]"
       id
       (print_separated_list print_type ", ") (Array.to_list a)
  | Topt t ->
     (match t with
      | Topt u -> print_type ff t
      | Tclass _ -> fprintf ff "@[%a?@]" print_type t
      | _ -> fprintf ff "@[(%a)?]" print_type t)
  | Tfun (l, t) ->
     fprintf ff "@[(%a) -> %a@]"
       (print_separated_list print_type ", ") l
       print_type t

and print_param ff (id, t) =
  fprintf ff "@[%s : %a@]"
    id
    print_type t

and print_var ff v =
  fprintf ff "@[%s %s : %a = %a ;@]@."
    (if v.v_is_mutable then "var" else "val")
    v.v_name
    print_type v.v_type
    print_expr v.v_expr

let print_param_c ff (p, is_mutable) =
  fprintf ff "@[%s %a@]"
    (if is_mutable then "var" else "val")
    print_param p

let print_fun ff f =
  fprintf ff "@.@[fun<%a> %s(%a) : %a %a@]@.@."
    (print_separated_list print_ident ", ") (Array.to_list f.f_type_params)
    f.f_name
    (print_separated_list print_param ", ") f.f_params
    print_type f.f_type
    print_bloc f.f_body

let print_class ff c =
  fprintf ff "@.@[data class %s<%a>(%a) : {@."
    c.c_name
    (print_separated_list print_ident ", ") (Array.to_list c.c_type_params)
    (print_separated_list print_param_c ", ") c.c_params ;
  print_tab () ;
  fprintf ff "@[%a@]@.}@]@.@."
    (print_separated_list print_var " ;@.") c.c_body
         
let rec print_file ff = function
  | [] -> ()
  | (Dvar v)::q -> print_var ff v ;
                   print_file ff q
  | (Dclass c)::q -> print_class ff c ;
                     print_file ff q
  | (Dfun f)::q -> print_fun ff f ;
                   print_file ff q




