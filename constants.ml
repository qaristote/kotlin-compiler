open Ast

let type_null = Tclass ("Null", [||])
let type_int = Tclass ("Int", [||])
let type_string = Tclass ("String", [||])
let type_bool = Tclass ("Boolean", [||])
let type_unit = Tclass ("Unit", [||])
let type_any = Tclass ("Any", [||])
let type_array t = Tclass ("Array", [|t|])

let type_opt = function
  | t when t = type_null -> type_null
  | Topt t -> Topt t
  | t -> Topt t
