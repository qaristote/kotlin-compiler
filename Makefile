CMO=constants.cmo lexer.cmo parser.cmo printer.cmo typer.cmo x86_64.cmo compiler.cmo main.cmo
GENERATED=lexer.ml parser.ml parser.mli
BIN=pkotlinc
FLAGS=-dtypes

all: $(BIN)

$(BIN):$(CMO)
	ocamlc $(FLAGS) -o $(BIN) $(CMO)

.SUFFIXES: .mli .ml .cmi .cmo .mll .mly

.mli.cmi:
	ocamlc $(FLAGS) -c  $<

.ml.cmo:
	ocamlc $(FLAGS) -c  $<

.mll.ml:
	ocamllex $<

.mly.ml:
	menhir --infer -v $<

.mly.mli:
	menhir -v $<

clean:
	rm -f *.cm[io] *.o *.annot *~ $(BIN) $(GENERATED)
	rm -f parser.automaton parser.conflicts

.depend depend: $(GENERATED)
	rm -f .depend
	ocamldep *.ml *.mli > .depend

include .depend
parser.ml: ast.cmi constants.cmo
