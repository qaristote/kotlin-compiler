(* Modules *)

open Ast
open Printer
open Constants
open Format

module IdSet = Set.Make(String)
type object_hashtbl = (ident, typ) Hashtbl.t
type mutables_hashtbl = (ident, bool) Hashtbl.t
type fields_hashtbl = (ident, (ident, typ * bool) Hashtbl.t) Hashtbl.t
type generics_hashtbl = (ident, ident array) Hashtbl.t
                     
type environment = {objects :  object_hashtbl ;
                    (* types des objets *)
                    alternative : object_hashtbl ;
                    (* types alternatifs *)
                    mutables :  mutables_hashtbl ;
                    (* ensemble des variables mutables *)
                    fields : fields_hashtbl ;
                    (* champs des classes : type, est mutable *)
                    generics : generics_hashtbl ;
                    (* variables de types triées dans leur ordre d'apparition *)}


                 
(* Déclaration des erreurs *)

exception No_main_method
(* le fichier ne contient pas de fonction main adéquate *)
        
exception Wrong_type of loc * typ list * typ
(* position, types attendus, type obtenu *)
(* mauvais type *)

exception Immutable_object of loc * ident
(* position, nom de la variable *)
(* variable non mutable *)

exception Unbound_object of loc * ident
(* position, nom de l'objet *)
(* l'objet n'a pas été défini *)

exception Declaration_conflict of loc * ident
(* position, nom de l'objet *)
(* un objet de même nom existe déjà *)

exception Unauthorized_return of loc
(* position *)
(* retour effectué à l'extérieur de tous les blocs de fonctions *)

exception Wrong_param_number of loc
(* position *)
(* trop ou pas assez d'arguments passés *)

exception Unsound_type of loc * typ
(* position, type *)
(* le type n'est pas bien fondé *)

exception Unresolved_type_param of loc * ident
(* position, nom du paramètre de type *)
(* impossible de déduire une valeur pour le paramètre de type lors de l'appel *)

exception Error of loc option * string
(* position, message d'erreur *)
(* erreur de typage *)
                 
                                
(* Code *)
                 
(* ('a -> 'b -> bool) -> 'a array -> 'b array -> bool *)
let for_all2 foo a1 a2 =
  let rec aux i =
    try
      if foo a1.(i) a2.(i) then
        aux (i + 1)
      else
        false
    with
    | Invalid_argument _ -> true in
  aux 0

(* environment -> typ -> bool *)
(* vérifie qu'un type est bien fondé *)
let rec is_sound env = function (* ✓ *)
  | t when t = type_any -> true
  | Tclass (id, sigma) ->
     Hashtbl.mem env.fields id
     && Array.length (Hashtbl.find env.generics id) = Array.length sigma
     && Array.fold_left (fun b t -> b && is_sound env t) true sigma
  | Topt t -> is_sound env t
  | Tfun (l, t) -> is_sound env t && List.for_all (is_sound env) l
  
(* typ -> typ -> bool *)
(* détermine si un type est sous-type d'un second *)
let rec is_subtype t u = match t, u with
  | Topt t1, Topt t2 -> is_subtype t1 (Topt t2)
  | t1, Topt t2 -> t1 = type_null || is_subtype t1 t2
  | Tclass (id1, sigma1), Tclass (id2, sigma2) ->
     id1 = id2 && Array.length sigma1 = Array.length sigma2 &&
       for_all2 is_subtype sigma1 sigma2
  | Tfun (args1, t1), Tfun (args2, t2) ->
     is_subtype t1 t2 && (try
                              List.for_all2 is_subtype args1 args2
                            with
                            | Invalid_argument _ -> false)
  | _ -> false

(* typ -> typ -> typ *)
(* détermine le plus petit type dont deux types sont sous-types *)
(* si les types ne sont pas compatibles, lève l'exception Invalid_argument *)
let rec max_type t u = match t, u with
  | t1, t2 when is_subtype t1 t2 -> t2
  | t1, t2 when is_subtype t2 t1 -> t1
  | (Topt t1, Topt t2 | t1, Topt t2 | Topt t1, t2) -> 
     (* le plus petit type contenant t1? et t2 (par 
        exemple) contient à la fois t1, t2 et Null *)
     type_opt (max_type t1 t2)
  | t1, t2 when t1 = type_null -> Topt t2
  | t1, t2 when t2 = type_null -> Topt t1
  | Tclass (c1, sigma1), Tclass (c2, sigma2) when c1 = c2 ->
     Tclass (c1, Array.mapi (fun i t -> max_type sigma1.(i) t) sigma2)
  | Tfun (args1, t1), Tfun (args2, t2) ->
     Tfun (List.map2 max_type args1 args2, max_type t1 t2)
  | _ -> raise (Invalid_argument "")

(* varexpr list -> bool *)
(* détermine si toutes les branches d'un bloc contiennent un retour *)
let rec check_return = function
  | [] -> false
  | Expr {e_instr = Ereturn _} :: q -> true
  | Expr {e_instr = Eif (_, b1, b2)} :: q ->
     (check_return b1.b_instr && check_return b2.b_instr) || check_return q
  | _ :: q -> check_return q

(* (ident, typ) Hashtbl.t -> typ -> typ *)
(* détermine la valeur d'un paramètre de type *)
let rec evaluate_type type_values = function
  | Tclass (id, [||]) when Hashtbl.mem type_values id ->
     Hashtbl.find type_values id
  | Tclass (id, sigma) ->
     Tclass (id, Array.map (evaluate_type type_values) sigma)
  | Topt t -> type_opt (evaluate_type type_values t)
  | Tfun (l, t) ->
     Tfun (List.map (evaluate_type type_values) l, evaluate_type type_values t)

(* (ident, typ) Hashtbl.t -> typ -> unit *)
(* unifie deux types dont seul le premier peut être polymorphe *)
(* pour unifier les types t et u, il faut faire en sorte que le 
   type u soit inclus dans le type t *)
let rec match_types type_values t u = match t, u with
  | Tclass (id, [||]), t when Hashtbl.mem type_values id ->
     let value = Hashtbl.find type_values id in
     if value = type_any then 
       Hashtbl.replace type_values id t
     else
       Hashtbl.replace type_values id (max_type value t)
  | Topt t1, Topt t2 -> match_types type_values (Topt t1) t2
  | Topt t1, t2 -> if t2 <> type_null then
                     match_types type_values t1 t2
  | t1, Topt t2 -> match_types type_values t1 type_null ;
                   match_types type_values t1 t2
  | Tclass (id1, sigma1), Tclass (id2, sigma2)
       when id1 = id2
            && Array.length sigma1 = Array.length sigma2 ->
     Array.iteri (fun i t -> match_types type_values t (sigma2.(i))) sigma1
  | Tfun (l1, t1), Tfun (l2, t2) -> match_types type_values t1 t2 ;
                                    List.iter2 (match_types type_values) l1 l2
  | _ -> raise (Invalid_argument "")

(* environment -> expr -> (id * typ * typ) option *)
(* renvoie un environnement dans lequel on a précisé 
   le type de certaines variables lorsque possible *)
let rec check_nullity env = function
  | Eunop (Unot, e) ->
     (match e.e_instr with
      | Ebinop (Bor, e1, e2) ->
         check_nullity env (Ebinop
                              (Band,
                               {e_loc = e1.e_loc ;
                                e_type = e1.e_type ;
                                e_instr = Eunop (Unot, e1)},
                               {e_loc = e2.e_loc ;
                                e_type = e2.e_type ;
                                e_instr = Eunop (Unot, e2)}))
      | Ebinop (Bis, e1, e2) ->
         check_nullity env (Ebinop (Bnis, e1, e2))
      | Ebinop (Bnis, e1, e2) ->
         check_nullity env (Ebinop (Bis, e1, e2))
      | Eunop (Unot, f) ->
         check_nullity env f.e_instr
      | _ -> [])
  | (Ebinop (o, {e_instr = Ecst Cthis}, {e_instr = Ecst Cnull})
     | Ebinop (o, {e_instr = Ecst Cnull}, {e_instr = Ecst Cthis})) ->
     (match Hashtbl.find env.objects "this" with
      | Topt t -> Hashtbl.add
                    env.objects
                    "this"
                    (if o = Bis then
                       type_null
                     else
                       t) ;
                  Hashtbl.add env.alternative "this" (Topt t) ;
                  ["this"]
      | t -> Hashtbl.add env.objects "this" (if o = Bis then
                                           type_null
                                         else
                                           t) ;
             Hashtbl.add env.alternative "this" t ;
             ["this"])
  | (Ebinop (o, {e_instr = Eget (Gident id)}, {e_instr = Ecst Cnull})
     | Ebinop (o, {e_instr = Ecst Cnull}, {e_instr = Eget (Gident id)})) ->
     (match Hashtbl.find env.objects id with
      | Topt t -> Hashtbl.add
                    env.objects
                    id
                    (if o = Bis then
                       type_null
                     else
                       t) ;
                  Hashtbl.add env.alternative id (Topt t) ;
                  [id]
      | t -> Hashtbl.add env.objects id (if o = Bis then
                                           type_null
                                         else
                                           t) ;
             Hashtbl.add env.alternative id t ;
             [id])
  | Ebinop (Band, e1, e2) -> check_nullity env e1.e_instr
                             @ check_nullity env e2.e_instr
  | _ -> []

(* environment -> list -> unit *)
(* s'utilise après check_nullity pour restaurer l'environnement *)
let restore env = 
  List.iter (fun id -> Hashtbl.remove env.objects id ;
                       Hashtbl.remove env.alternative id)
                       

(* environment -> expr -> expr *)       
(* type une expression *)
let rec type_expr env expr =
  let instr, typ = match expr.e_instr with

    | Ecst c -> Ecst c, (match c with
                    | Cnull -> type_null
                    | Cthis -> if Hashtbl.mem env.objects "this" then
                                 Hashtbl.find env.objects "this"
                               else
                                 raise (Unbound_object (expr.e_loc, "this"))
                    | Cint _ -> type_int
                    | Cstring _ -> type_string
                    | Cbool _ -> type_bool)

    | Eget g ->
       (match g with
        | Gident id -> if Hashtbl.mem env.objects id then
                         Eget g, Hashtbl.find env.objects id
                       else
                         raise (Unbound_object (expr.e_loc, id))
        | Gatt (e, id) ->
           let new_e = type_expr env e in
           (match new_e.e_type with
            | Tclass (c_id, sigma)
                 when (Hashtbl.mem (Hashtbl.find env.fields c_id) id) ->
               let type_values = Hashtbl.create (Array.length sigma) in
               Array.iteri
                 (fun i id -> Hashtbl.add type_values id sigma.(i))
                 (Hashtbl.find env.generics c_id) ;
               Eget (Gatt (new_e, id)),
               evaluate_type
                 type_values
                 (fst (Hashtbl.find (Hashtbl.find env.fields c_id) id))
            | t -> raise (Unbound_object (expr.e_loc, id)))
        | Goptatt (e, id) ->
           let new_e = type_expr env e in
           (match new_e.e_type with
            | Tclass (c_id, sigma)
                 when (Hashtbl.mem (Hashtbl.find env.fields c_id) id) ->
               let type_values = Hashtbl.create (Array.length sigma) in
               Array.iteri
                 (fun i id -> Hashtbl.add type_values id sigma.(i))
                 (Hashtbl.find env.generics c_id) ;
               Eget (Gatt (new_e, id)),
               evaluate_type
                 type_values
                 (fst (Hashtbl.find (Hashtbl.find env.fields c_id) id))
            | Topt (Tclass (c_id, sigma))
                 when (Hashtbl.mem (Hashtbl.find env.fields c_id) id) ->
               let type_values = Hashtbl.create (Array.length sigma) in
               Array.iteri
                 (fun i id -> Hashtbl.add type_values id sigma.(i))
                 (Hashtbl.find env.generics c_id) ;
               Eget (Gatt (new_e, id)),
               type_opt
                 (evaluate_type
                    type_values
                    (fst (Hashtbl.find (Hashtbl.find env.fields c_id) id)))
            | t -> raise (Unbound_object (expr.e_loc, id))))

    | Eset (g, e) ->
       let new_e = type_expr env e in
       (match g with
        | Gident id ->
           if Hashtbl.mem env.objects id then (
             if Hashtbl.find env.mutables id then (
               let t = Hashtbl.find env.objects id in
               if (Hashtbl.mem env.alternative id
                   && is_subtype new_e.e_type (Hashtbl.find env.alternative id))
                   || is_subtype new_e.e_type t then
                 Eset (g, new_e), type_unit
               else
                 raise (Wrong_type (new_e.e_loc,
                                    [t],
                                    new_e.e_type))
             ) else
               raise (Immutable_object (expr.e_loc, id))
           ) else
             raise (Unbound_object (expr.e_loc, id))
        | Gatt (f, id) ->
           let new_f = type_expr env f in
           (match new_f.e_type with
            | Tclass (c_id, sigma)
                 when (Hashtbl.mem (Hashtbl.find env.fields c_id) id) ->
               let fields = Hashtbl.find env.fields c_id in
               let t, is_mutable = Hashtbl.find fields id in
               if is_mutable then (
                 let type_values = Hashtbl.create (Array.length sigma) in
                 Array.iteri
                   (fun i id -> Hashtbl.add type_values id sigma.(i))
                   (Hashtbl.find env.generics c_id) ;
                 if is_subtype new_e.e_type (evaluate_type
                                               type_values
                                               t) then
                   Eset (Gatt (new_e, id), new_e), type_unit
                 else
                   raise (Wrong_type (new_e.e_loc,
                                      [t],
                                      new_e.e_type))
               ) else 
                 raise (Immutable_object (expr.e_loc, id))
            | t -> raise (Unbound_object (expr.e_loc, id)))
        | Goptatt (f, id) ->
           let new_f = type_expr env f in
           (match new_f.e_type with
            | Tclass (c_id, sigma)
                 when (Hashtbl.mem (Hashtbl.find env.fields c_id) id) ->
               let fields = Hashtbl.find env.fields c_id in
               let t, is_mutable = Hashtbl.find fields id in
               if is_mutable then (
                 let type_values = Hashtbl.create (Array.length sigma) in
                 Array.iteri
                   (fun i id -> Hashtbl.add type_values id sigma.(i))
                   (Hashtbl.find env.generics c_id) ;
                 if is_subtype new_e.e_type (evaluate_type
                                               type_values
                                               t) then
                   Eset (Goptatt (new_e, id), new_e), type_unit
                 else
                   raise (Wrong_type (new_e.e_loc,
                                      [t],
                                      new_e.e_type))
               ) else 
                 raise (Immutable_object (expr.e_loc, id))
            | Topt (Tclass (c_id, sigma))
                 when (Hashtbl.mem (Hashtbl.find env.fields c_id) id) ->
               let fields = Hashtbl.find env.fields c_id in
               let t, is_mutable = Hashtbl.find fields id in
               if is_mutable then (
                 let type_values = Hashtbl.create (Array.length sigma) in
                 Array.iteri
                   (fun i id -> Hashtbl.add type_values id sigma.(i))
                   (Hashtbl.find env.generics c_id) ;
                 if is_subtype new_e.e_type (evaluate_type
                                               type_values
                                               t) then
                   Eset (Gatt (new_e, id), new_e), type_unit
                 else
                   raise (Wrong_type (new_e.e_loc,
                                      [t],
                                      new_e.e_type))
               ) else 
                 raise (Immutable_object (expr.e_loc, id))
            | t -> raise (Unbound_object (expr.e_loc, id))))

    | Ecall ("print", [arg]) ->
       let new_arg = type_expr env arg in
       if new_arg.e_type = type_int
          || is_subtype new_arg.e_type (Topt type_string) then
         Ecall ("print", [new_arg]), type_unit
       else
         raise (Wrong_type (new_arg.e_loc,
                            [type_int ; Topt type_string],
                            new_arg.e_type))

    | Ecall (id, params) ->
       if Hashtbl.mem env.objects id then
         match Hashtbl.find env.objects id with
         | Tfun (params_types, t) ->
            (try
               let generics = try
                   Hashtbl.find env.generics id
                 with
                 | Not_found -> [||] in
               let type_values = Hashtbl.create 0 in
               Array.iter
                 (fun id -> Hashtbl.replace type_values id type_any) generics ;
               let new_instr =
                 Ecall (id, List.map2
                              (fun t arg ->
                                let new_arg = type_expr env arg in
                                let arg_t = match new_arg with
                                  | {e_type = t ;
                                     e_instr = Eget (Gident s)}
                                       when t = type_null
                                            && Hashtbl.mem env.alternative s ->
                                     Hashtbl.find env.alternative s
                                  | _ -> new_arg.e_type in
                                (try
                                   match_types type_values t arg_t
                                 with
                                 | Invalid_argument _ ->
                                    raise (Wrong_type (new_arg.e_loc,
                                                       [t],
                                                       arg_t))) ;
                                new_arg)
                              params_types
                              params) in
               for i = 0 to Array.length generics - 1 do
                 if Hashtbl.find type_values generics.(i) = type_any then
                   raise (Unresolved_type_param (expr.e_loc, generics.(i)))
               done ;
               new_instr, evaluate_type type_values t
             with
             | Invalid_argument _ ->
                raise (Wrong_param_number (expr.e_loc)))
         | t -> raise (Wrong_type (expr.e_loc,
                                   [Tfun ([type_any], type_any)],
                                   t))
       else
         raise (Unbound_object (expr.e_loc, id))
                            
                            
    | Eunop (o, e) ->
       let new_e = type_expr env e in
       let t = match o with
         | Unot -> type_bool
         | Uneg -> type_int in
       if new_e.e_type = t then
         Eunop (o, new_e), t
       else
         raise (Wrong_type (new_e.e_loc,
                            [t],
                            new_e.e_type))

    | Ebinop (o, e1, e2) ->
       let new_e1 = type_expr env e1 in
       let new_e2 = if o = Band || o = Bor then (
         let vars = check_nullity
                      env
                      (if o = Band then
                         e1.e_instr
                       else
                         Eunop (Unot,
                                ({e_loc = e1.e_loc ;
                                    e_type = e1.e_type ;
                                    e_instr = e1.e_instr}))) in
                    let tmp_e2 = type_expr env e2 in
                    restore env vars ;
                    tmp_e2
                    ) else
                      type_expr env e2 in
       Ebinop (o, new_e1, new_e2),
       (match o with
        | (Bis | Bnis) ->
           if new_e1.e_type <> type_unit
              && new_e1.e_type <> type_int
              && new_e1.e_type <> type_bool then (
             if new_e2.e_type <> type_unit
                && new_e2.e_type <> type_int
                && new_e2.e_type <> type_bool then
               type_bool
             else
               raise (Wrong_type (new_e2.e_loc,
                                  [type_null;
                                   type_string;
                                   type_array type_any;
                                   Tclass ("Any", [|type_any|]);
                                   Tfun ([type_any], type_any)],
                                  new_e2.e_type))
           ) else (
             raise (Wrong_type (new_e1.e_loc,
                                  [type_null;
                                   type_string;
                                   type_array type_any;
                                   Tclass ("Any", [|type_any|]);
                                   Tfun ([type_any], type_any)],
                                  new_e1.e_type)))
        | (Band | Bor) ->
           if new_e1.e_type = type_bool then (
             if new_e2.e_type = type_bool then 
               type_bool
             else
               raise (Wrong_type (new_e2.e_loc,
                                  [type_bool],
                                  new_e2.e_type))
           ) else (
             raise (Wrong_type (new_e1.e_loc,
                                [type_bool],
                                new_e1.e_type)))
        | (Badd | Bsub | Bmul | Bdiv | Bmod) ->
           if new_e1.e_type = type_int then (
             if new_e2.e_type = type_int then
               type_int
             else
               raise (Wrong_type (new_e2.e_loc,
                                  [type_int],
                                  new_e2.e_type))
           ) else (
             raise (Wrong_type (new_e1.e_loc,
                                [type_int],
                                new_e1.e_type)))
        | (Blt | Ble | Beq | Bneq | Bge | Bgt) ->
           if new_e1.e_type = type_int then (
             if new_e2.e_type = type_int then
               type_bool
             else
               raise (Wrong_type (new_e2.e_loc,
                                  [type_int],
                                  new_e2.e_type))
           ) else (
             raise (Wrong_type (new_e1.e_loc,
                                [type_int],
                                new_e1.e_type))))

    | Eif (e, b1, b2) ->
       let new_e = type_expr env e in
       if new_e.e_type = type_bool then (
         let vars_if_true = check_nullity env new_e.e_instr in
         let new_b1 = type_bloc env b1 in
         restore env vars_if_true ;
         let vars_if_false =
           check_nullity env (Eunop (Unot, {e_loc = new_e.e_loc ;
                                            e_type = new_e.e_type ;
                                            e_instr = new_e.e_instr})) in
         let new_b2 = type_bloc env b2 in
         restore env vars_if_false ;
         if check_return b1.b_instr then (
           ignore (check_nullity env (Eunop (Unot, {e_loc = new_e.e_loc ;
                                                    e_type = new_e.e_type ;
                                                    e_instr = new_e.e_instr})))
         ) else if check_return b2.b_instr then (
           ignore (check_nullity env new_e.e_instr)) ;
         Eif (new_e, new_b1, new_b2),
         try
           max_type new_b1.b_type new_b2.b_type
         with
         | Invalid_argument _ -> raise (Wrong_type (new_b2.b_loc,
                                                    [new_b1.b_type],
                                                    new_b2.b_type))
       ) else
         raise (Wrong_type (new_e.e_loc,
                            [type_bool],
                            new_e.e_type))

    | Ewhile (e, b) ->
       let new_e = type_expr env e in
       if new_e.e_type = type_bool then (
         let vars_if_true = check_nullity env new_e.e_instr in
         let new_b = type_bloc env b in
         restore env vars_if_true ;
         ignore (check_nullity env (Eunop (Unot, {e_loc = new_e.e_loc ;
                                            e_type = new_e.e_type ;
                                            e_instr = new_e.e_instr}))) ;
         Ewhile (new_e, new_b), type_unit
       ) else
         raise (Wrong_type (new_e.e_loc,
                            [type_bool],
                            new_e.e_type))

    | Ereturn eopt ->
       let new_eopt, return_type = match eopt with
         | Some e -> let new_e = type_expr env e in
                     Some new_e, new_e.e_type
         | None -> None, type_unit in
       if Hashtbl.mem env.objects "return" then (
         if is_subtype return_type (Hashtbl.find env.objects "return") then
           Ereturn new_eopt, type_unit
         else
           raise (Wrong_type (expr.e_loc,
                              [Hashtbl.find env.objects "return"],
                              return_type))
       ) else
         raise (Unauthorized_return expr.e_loc)
                                              

    | Efun (params, f_type, body) ->
       if is_sound env f_type then (
         if f_type = type_unit || check_return body.b_instr then (
           let local_vars = ref IdSet.empty in
           let params_types =
             List.map
               (fun (id, t) -> if IdSet.mem id !local_vars then
                                 raise (Declaration_conflict (expr.e_loc, id))
                               else (
                                 if is_sound env t then (
                                   local_vars := IdSet.add id !local_vars ;
                                   Hashtbl.add env.objects id t ;
                                   Hashtbl.add env.mutables id false ;
                                   t
                                 ) else
                                   raise (Unsound_type (expr.e_loc, t))))
               params in
           Hashtbl.add env.objects "return" f_type ;
           let new_body = type_bloc env body in
           Hashtbl.remove env.objects "return" ;
           IdSet.iter (fun id -> Hashtbl.remove env.objects id ;
                                 Hashtbl.remove env.mutables id)
             !local_vars ;
           Efun (params, f_type, new_body), Tfun (params_types, f_type)
       ) else
           raise (Wrong_type (body.b_loc,
                              [f_type],
                              type_unit))
       ) else
         raise (Unsound_type (expr.e_loc, f_type))
                         
  in {e_loc = expr.e_loc ;
      e_type = typ ;
      e_instr = instr}

(* environment -> bloc -> bloc *)
(* type un bloc *)
and type_bloc env b =
  let local_alts =
    Hashtbl.fold
      (fun id t set -> if not (IdSet.mem id set) then (
                         Hashtbl.add env.objects id (Hashtbl.find env.objects id) ;
                         Hashtbl.add env.alternative id t ;
                         IdSet.add id set)
                       else
                         set)
      env.alternative
      IdSet.empty in
  let local = ref IdSet.empty in
  let rec aux acc typ = function
    | [] -> List.rev acc, typ
    | Var v :: q -> if IdSet.mem v.v_name !local then
                      raise (Declaration_conflict (v.v_loc, v.v_name))
                    else (
                      local := IdSet.add v.v_name !local ;
                      aux (Var (type_var env v) :: acc) type_unit q)
    | Expr e :: q -> let new_e = type_expr env e in
                     aux (Expr new_e :: acc) new_e.e_type q in

  (* cf la différence entre Hashtbl.add et Hashtbl.replace *)
  
  let instr, typ = aux [] type_unit b.b_instr in
  IdSet.iter
    (fun id -> Hashtbl.remove env.objects id;
               Hashtbl.remove env.mutables id)
    !local ;
  IdSet.iter
    (fun id -> Hashtbl.remove env.objects id ;
               Hashtbl.remove env.alternative id)
    local_alts ;
  {b_loc = b.b_loc ;
   b_type = typ ;
   b_instr = instr}

(* environment -> var -> var *)
(* type une déclaration de variable *)
and type_var env v =
  if is_sound env v.v_type then (
    let e = type_expr env v.v_expr in
    let t = if v.v_type = type_any then
              e.e_type
            else (
              if is_subtype e.e_type v.v_type then
                v.v_type
              else
                raise (Wrong_type (v.v_loc,
                                   [v.v_type],
                                   e.e_type))) in
    Hashtbl.add env.objects v.v_name t ;
    Hashtbl.add env.mutables v.v_name v.v_is_mutable ;
    {v_loc = v.v_loc ;
     v_name = v.v_name ;
     v_type = t ;
     v_is_mutable = v.v_is_mutable ;
     v_expr = e}
  ) else
    raise (Unsound_type (v.v_loc, v.v_type))

(* environment -> clas -> clas *)
(* type une déclaration de classe *)
let type_class env c =
  if Hashtbl.mem env.objects c.c_name then
    raise (Declaration_conflict (c.c_loc, c.c_name))
  else (
    let local_vars = ref IdSet.empty in
    let fields = Hashtbl.create 0 in
    Hashtbl.replace env.fields c.c_name fields ;
    Hashtbl.replace env.generics c.c_name c.c_type_params ;
    let local_types = ref IdSet.empty in
    let types_from_params = 
    Array.map (fun id -> if IdSet.mem id !local_types then
                            raise (Declaration_conflict (c.c_loc, id))
                          else (
                            local_types := IdSet.add id !local_types ;
                            Hashtbl.add env.fields id (Hashtbl.create 0) ;
                            Hashtbl.add env.generics id [||]) ;
                          Tclass (id, [||]))
      c.c_type_params in
    Hashtbl.replace
      env.objects
      c.c_name
      (Tfun (List.map
               (fun ((id, t), b) -> if IdSet.mem id !local_vars then
                                      raise (Declaration_conflict (c.c_loc, id))
                                    else (
                                      if is_sound env t then (
                                        local_vars := IdSet.add id !local_vars ;
                                        Hashtbl.add fields id (t, b) ;
                                        Hashtbl.add env.objects id t ;
                                        Hashtbl.add env.mutables id b ;
                                        t
                                      ) else
                                        raise (Unsound_type (c.c_loc, t))))
               c.c_params,
             Tclass (c.c_name, types_from_params))) ;
    Hashtbl.add env.objects "this" (Tclass (c.c_name, types_from_params)) ;
    Hashtbl.add env.mutables "this" false ;
    let b =
      List.map
        (fun v -> if IdSet.mem v.v_name !local_vars then
                    raise (Declaration_conflict (v.v_loc, v.v_name))
                  else (
                    let new_v = type_var env v in
                    local_vars := IdSet.add v.v_name !local_vars ;
                    Hashtbl.replace fields v.v_name (v.v_type, v.v_is_mutable) ;
                    new_v))
        c.c_body in
    IdSet.iter (fun id -> Hashtbl.remove env.objects id ;
                          Hashtbl.remove env.mutables id)
      !local_vars ;
    IdSet.iter (fun id -> Hashtbl.remove env.fields id ;
                          Hashtbl.remove env.generics id)
      !local_types ;
    Hashtbl.remove env.objects "this" ;
    Hashtbl.remove env.mutables "this" ;
    {c_loc = c.c_loc ;
     c_name = c.c_name ;
     c_type_params = c.c_type_params ;
     c_params = c.c_params ;
     c_body = b})
     
    

(* environement -> func -> func *)
(* type une déclaration de fonction *)
let type_fun env f =
  if Hashtbl.mem env.objects f.f_name then
    raise (Declaration_conflict (f.f_loc, f.f_name))
  else (
    if f.f_type = type_unit || check_return f.f_body.b_instr then (
      Hashtbl.add env.generics f.f_name f.f_type_params ;
      let local_types = ref IdSet.empty in
      Array.iter (fun id -> if IdSet.mem id !local_types then
                              raise (Declaration_conflict (f.f_loc, id))
                            else (
                              local_types := IdSet.add id !local_types ;
                              Hashtbl.add env.fields id (Hashtbl.create 0) ;
                              Hashtbl.add env.generics id [||]))
        f.f_type_params ;
      if is_sound env f.f_type then (
        let local_vars = ref IdSet.empty in
        Hashtbl.replace
          env.objects
          f.f_name
          (Tfun (List.map
                   (fun (id, t) -> if IdSet.mem id !local_vars then
                                     raise (Declaration_conflict (f.f_loc, id))
                                   else (
                                     if is_sound env t then (
                                       local_vars := IdSet.add id !local_vars ;
                                       Hashtbl.add env.objects id t ;
                                       Hashtbl.add env.mutables id false ;
                                       t
                                     ) else
                                       raise (Unsound_type (f.f_loc, t))))
                   f.f_params,
                 f.f_type)) ;
        Hashtbl.add env.objects "return" f.f_type ;
        let b = type_bloc env f.f_body in
        Hashtbl.remove env.objects "return" ;
        IdSet.iter (fun id -> Hashtbl.remove env.objects id ;
                              Hashtbl.remove env.mutables id)
          !local_vars ;
        IdSet.iter (fun id -> Hashtbl.remove env.fields id ;
                              Hashtbl.remove env.generics id)
          !local_types ;
        {f_loc = f.f_loc ;
         f_name = f.f_name ;
         f_type_params = f.f_type_params ;
         f_params = f.f_params ;
         f_type = f.f_type ;
         f_body = b}
      ) else
        raise (Wrong_type (f.f_body.b_loc,
                           [f.f_type],
                           type_unit))
    ) else
      raise (Unsound_type (f.f_loc, f.f_type)))
    
(* file -> file *)
(* type un fichier *)
let type_file file =
  let env = {objects = Hashtbl.create 7 ;
             alternative = Hashtbl.create 0 ;
             mutables = Hashtbl.create 7;
             fields = Hashtbl.create 7 ;
             generics = Hashtbl.create 7} in
  Hashtbl.replace env.fields "Null"    (Hashtbl.create 0) ;
  Hashtbl.replace env.fields "Unit"    (Hashtbl.create 0) ;
  Hashtbl.replace env.fields "Boolean" (Hashtbl.create 0) ;
  Hashtbl.replace env.fields "Int"     (Hashtbl.create 0) ;
  Hashtbl.replace env.fields "String"  (Hashtbl.create 0) ;
  Hashtbl.replace env.fields "Array"   (Hashtbl.create 0) ;
  Hashtbl.replace env.generics "Null"    [||] ;
  Hashtbl.replace env.generics "Unit"    [||] ;
  Hashtbl.replace env.generics "Boolean" [||] ;
  Hashtbl.replace env.generics "Int"     [||] ;
  Hashtbl.replace env.generics "String"  [||] ;
  Hashtbl.replace env.generics "Array"   [|"T"|] ;
  Hashtbl.replace
    env.objects
    "Array"
    (Tfun ([Tclass ("T1", [||])], type_array (Tclass ("T ", [||])))) ;
  let rec type_decl acc = function
  | [] -> raise No_main_method
  | Dvar v :: q -> if Hashtbl.mem env.objects v.v_name then
                     raise (Declaration_conflict (v.v_loc, v.v_name))
                   else
                     type_decl (Dvar (type_var env v) :: acc) q
  | Dclass c :: q -> type_decl (Dclass (type_class env c) :: acc) q
  | Dfun f :: q ->
     (match type_fun env f with
      | {f_name = "main" ;
         f_type_params = [||] ;
         f_params = [(_, Tclass ("Array", [|type_string|]))] ;
         f_type = type_unit ;
         _ }
        as new_f -> List.rev (Dfun new_f :: acc)
      | {f_name = "main" ; _ } -> raise No_main_method
      | new_f -> type_decl (Dfun new_f :: acc) q) in
  try
    type_decl [] file, env
  with
  | No_main_method -> raise (Error (None, "no main method found in file."))
  | Wrong_type (loc, expected, got) ->
     raise (Error (Some loc,
                   (fprintf
                      str_formatter
                     "type mismatch : inferred type is %a but %a was expected"
                     print_type got
                     (print_separated_list print_type "/") expected ;
                    flush_str_formatter ())))
  | Immutable_object (loc, id) ->
     raise (Error (Some loc,
                   (fprintf
                      str_formatter
                      "%s is immutable" id ;
                    flush_str_formatter ())))
  | Unbound_object (loc, id) ->
     raise (Error (Some loc,
                   (fprintf
                      str_formatter
                     "unresolved reference : %s" id ;
                    flush_str_formatter ())))
  | Declaration_conflict (loc, id) ->
     raise (Error (Some loc,
                   (fprintf
                      str_formatter
                      "conflicting declarations for %s" id ;
                    flush_str_formatter ())))
  | Unauthorized_return loc ->
     raise (Error (Some loc, "expecting a declaration"))
  | Wrong_param_number loc ->
     raise (Error (Some loc, "wrong number of parameters"))
  | Unsound_type (loc, t) ->
     raise (Error (Some loc,
                   (fprintf
                      str_formatter
                      "unsound type : %a"
                      print_type t ;
                    flush_str_formatter ())))
  | Unresolved_type_param (loc, id) ->
     raise (Error (Some loc, "cannot deduce value for type parameter " ^ id))
