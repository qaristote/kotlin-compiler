(* Arbres de syntaxe abstraite de Petit Kotlin *)
           
type ident = string
           
type loc = Lexing.position * Lexing.position (* beginning, end *)

type unop =
  | Uneg (* -e *)
  | Unot (* !e *)

type binop =
  | Badd | Bsub | Bmul | Bdiv | Bmod                (* + - * / % *)
  | Bis | Bnis | Beq | Bneq | Blt | Ble | Bgt | Bge (* === !== == != < <= > >= *)
  | Band | Bor                                      (* && || *)

type cst =
  | Cnull
  | Cthis
  | Cint    of int
  | Cstring of string
  | Cbool   of bool

type get =
  | Gident  of ident
  | Gatt    of expr * ident (* e.x *)
  | Goptatt of expr * ident (* e?.x *)

and varexpr =
  | Var  of var
  | Expr of expr
                                              
and bloc = {b_loc : loc ;
            b_type : typ ;
            b_instr : varexpr list}

and expr = {e_loc : loc;
            e_type : typ;
            e_instr : instr}

and instr =
  | Ecst    of cst
  | Eget    of get
  | Eset    of get * expr
  | Ecall   of ident * expr list
  | Eunop   of unop * expr
  | Ebinop  of binop * expr * expr
  | Eif     of expr * bloc * bloc
  | Ewhile  of expr * bloc
  | Ereturn of expr option
  | Efun    of param list * typ * bloc

and typ =
  | Tclass of ident * typ array
  | Topt   of typ
  | Tfun   of typ list * typ
          
and param = ident * typ

and var = {v_loc : loc ;
           v_name : ident ;
           v_type : typ ;
           v_expr : expr ;
           v_is_mutable : bool}

type param_c = param * bool
          
type func = {f_loc : loc;
             f_name : ident ;
             f_type_params : ident array ;
             f_params : param list ;
             f_type : typ ;
             f_body : bloc}
          
type clas = {c_loc : loc ;
             c_name : ident ;
             c_type_params : ident array ;
             c_params : param_c list ;
             c_body : var list}

type decl =
  | Dvar   of var
  | Dclass of clas
  | Dfun   of func
          
type file = decl list
