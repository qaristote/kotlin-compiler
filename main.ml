(* Programme principal *)

open Format
open Lexing

(* Options de compilation *)
let parse_only = ref false
let type_only = ref false
let print = ref false

(* Noms de fichiers source et cible *)
let ifile = ref ""
let ofile = ref ""

let set_file f s = f := s
          
(* Les options du compilateur affichées avec --help *)
let options =
  [
    "--parse-only", Arg.Set parse_only, " s'arrêter après le parsage" ;
    "--type-only", Arg.Set type_only, " s'arrêter après le typage"  ;
    "--print", Arg.Set print, " écrire le résultat du parsage dans un fichier"
  ]

let usage = "usage: pkotlinc [options] file.kt"

(* localise une erreur*)
let report (b, e) =
  let l = b.pos_lnum in
  let fc = b.pos_cnum - b.pos_bol + 1 in
  let lc = e.pos_cnum - b.pos_bol + 1 in
  eprintf "File \"%s\", line %d, characters %d-%d:\n" !ifile l fc lc

let file =
  (* Parsing de la ligne de commande *)
  Arg.parse options (set_file ifile) usage ;

  (* On vérifie que le nom du fichier source a bien été indiqué *)
  if !ifile = "" then (
    eprintf "No file to compile.\n@?";
    exit 1
  ) ;

  (* Ce fichier doit avoir l'extension .kt *)
  if not (Filename.check_suffix !ifile ".kt") then (
    eprintf "Input file must have .kt extension..\n@?" ;
    exit 1
  ) ;

  (* Ouverture du fichier source en lecture *)
  let f = open_in !ifile in

  (* Création d'un tampon d'analyse lexicale *)
  let buf = Lexing.from_channel f in

  try
    (* Parsage : la fonction Parser.file transforme le tampon lexical en un
       arbre de syntaxe abstraite si aucune erreur (lexicale ou syntaxique)
       n'est détectée.
       La fonctionn Lexer.token est utilisée par Parser.file pour obtenir le
       prochain token. *)
    let p = Parser.file Lexer.token buf in
    close_in f;

    if !print then (
      ofile := (Filename.chop_suffix !ifile ".kt") ^ "_parsed.kt" ;
      Printer.print_file (formatter_of_out_channel (open_out !ofile)) p
    ) ;

    (* On s'arrête ici si on ne veut faire que le parsage *)
    if !parse_only then exit 0 ;
    let typed_p, typing_env = Typer.type_file p in
                
    (* On s'arrête ici si on ne veut faire que le typage *)
    if !type_only then exit 0 ;
    let asm = Compiler.compile typed_p in
    ofile := (Filename.chop_suffix !ifile "kt") ^ "s" ;
    X86_64.print_in_file ~file:!ofile asm ;
    exit 0 ;
                
    
  with
  | Lexer.Lexing_error s ->
     (* Erreur lexicale. *)
     report (lexeme_start_p buf, lexeme_end_p buf);
     eprintf "Lexical error : %s@." s;
     exit 1
  | Parser.Error ->
     (* Erreur syntaxique. *)
     report (lexeme_start_p buf, lexeme_end_p buf);
     eprintf "Syntax error.@.";
     exit 1
  | Typer.Error (locopt, s) ->
        (* Erreur de typage. *)
     (match locopt with
      | Some loc -> report loc
      | None -> ()) ;
       eprintf "Typing error : %s@." s ;
        exit 1
   (* | Compiler.Error pos ->
       report pos ;
       eprintf "Erreur à la compilation@." ;
       exit 1*)
  | e ->
     (* Autre erreur. *)
     eprintf "Anomaly: %s\n@." (Printexc.to_string e);
     exit 2



